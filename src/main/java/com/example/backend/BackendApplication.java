package com.example.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@CrossOrigin()
	@GetMapping("/")
	public String getRequest(){
		return "Hello World!";
	}

	@CrossOrigin()
	@PostMapping("/sendData")
	public Request postRequest(@RequestBody Request req){
		return new Request(req.getCode(), req.getCheckBox());
	}
}

