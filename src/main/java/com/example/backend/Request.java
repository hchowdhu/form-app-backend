package com.example.backend;

public class Request {

    private String code;

    private String checkBox;

    private Integer lines;

    Request(String code, String checkBox) {
        this.code = code;
        this.checkBox = checkBox;
        this.lines = this.countLines(code);
    }

    public String getCode() {
        return code;
    }

    public String getCheckBox() {
        return checkBox;
    }

    public Integer getLines() {
        return lines;
    }

    public int countLines(String str){
        String[] lines = str.split("\r\n|\r|\n");
        return  lines.length;
    }
}
